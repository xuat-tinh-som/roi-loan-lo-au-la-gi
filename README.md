Nên chữa rối loạn lo âu ở đâu ?
Có thể phát triển chứng rối loạn lo âu tổng quát ở một đứa trẻ hoặc là một người lớn. Rối loạn lo âu tổng quát có các triệu chứng tương tự như rối loạn hoảng sợ, rối loạn ám ảnh cưỡng chế và các loại khác của sự lo âu, nhưng tất cả các điều kiện khác nhau.

Sống với rối loạn lo âu tổng quát có thể là một thách thức lâu dài. Trong nhiều trường hợp, nó xảy ra cùng với sự lo lắng khác hoặc các rối loạn tâm trạng. Trong hầu hết trường hợp, rối loạn lo âu tổng quát được cải thiện bằng thuốc hoặc tư vấn tâm lý. Thay đổi lối sống, học tập kỹ năng đối phó và sử dụng các kỹ thuật thư giãn cũng có thể giúp đỡ.

Các triệu chứng

Các triệu chứng rối loạn lo âu tổng quát có thể khác nhau. Có thể bao gồm:


Liên tục lo lắng, ám ảnh về mối quan tâm nhỏ hay lớn.
Bồn chồn.
Mệt mỏi.
Khó tập trung tâm trí.
Khó chịu.
Cơ bắp căng thẳng hoặc đau nhức bắp thịt.
Run rẩy, cảm thấy bối rối hoặc bị dễ dàng giật mình.
Khó ngủ.
Ra mồ hôi, buồn nôn hoặc tiêu chảy.
Khó thở hoặc nhịp tim nhanh.

http://yhoccotruyensaigon.com/roi-loan-lo-au-la-benh-gi-trieu-chung-nguyen-nhan-va-cach-chua-520.html

Có thể có lần khi lo lắng không hoàn toàn biến mất, nhưng vẫn cảm thấy lo lắng ngay cả khi không có lý do rõ ràng. Ví dụ, có thể cảm thấy lo lắng căng thẳng về sự an toàn hoặc của những người thân yêu, hoặc có thể có một cảm giác chung là một cái gì đó tồi tệ sắp xảy ra.

Các triệu chứng ở trẻ em và thanh thiếu niên


Ngoài các triệu chứng trên, trẻ em và thanh thiếu niên có thể có lo lắng quá nhiều về:
Hoạt động tại trường học hoặc các sự kiện thể thao.
Về thời gian (đúng giờ).
Động đất, chiến tranh hạt nhân hoặc sự kiện thảm khốc khác.
Một đứa trẻ bị rối loạn cũng có thể:
Cảm thấy quá lo lắng để phù hợp.
Là một người cầu toàn.
Thiếu tự tin.
Làm lại nhiệm vụ bởi vì họ không hoàn thiện lần đầu.
Phấn đấu phê duyệt.
Đòi hỏi rất nhiều sự bảo đảm về hiệu suất.
Một số lo lắng là bình thường, nhưng khám bác sĩ nếu:
Cảm thấy như đang lo lắng quá nhiều, và nó can thiệp vào công việc, các mối quan hệ hoặc các phần khác của cuộc sống.
Cảm thấy chán nản, có rắc rối với rượu hoặc ma túy, hoặc có vấn đề sức khỏe tâm thần khác cùng với lo âu.
Có ý nghĩ tự tử hoặc hành vi tìm kiếm sự điều trị khẩn cấp ngay lập tức.


Lo lắng không chỉ đơn giản( điều trị trầm cảm trong bao lâu ), và họ thực sự có thể trở nên tồi tệ hơn theo thời gian. Hãy thử tìm kiếm sự giúp đỡ chuyên nghiệp trước khi lo lắng trở nên trầm trọng - có thể dễ dàng hơn để điều trị sớm.

rối loạn lo âu

http://yhoccotruyensaigon.com/dia-chi-kham-va-chua-roi-loan-lo-au-bang-dong-y-o-dau-tot-tphcm-521.html

Nguyên nhân

Như với nhiều điều kiện sức khỏe tâm thần, những gì gây ra rối loạn lo âu tổng quát không hoàn toàn hiểu rõ. Nó có thể liên quan đến hóa chất trong não (chất dẫn truyền thần kinh), chẳng hạn như dopamine, serotonin và norepinephrine. Có khả năng tình trạng này có nhiều nguyên nhân có thể bao gồm di truyền học, kinh nghiệm cuộc sống và căng thẳng.

Một số điều kiện sức khỏe thể chất có liên quan với lo âu. Các ví dụ bao gồm:


Bệnh trào ngược dạ dày thực quản (GERD).
Bệnh tim.
Suy giáp hoặc cường giáp.
Thời kỳ mãn kinh.
Các yếu tố nguy cơ
Những điều có thể làm tăng nguy cơ phát triển chứng rối loạn lo âu tổng quát bao gồm:
Là phụ nữ. Phụ nữ hơn hai lần nhiều hơn nam giới được chẩn đoán mắc chứng rối loạn lo âu tổng quát.
Chấn thương thời thơ ấu. Những trẻ em đã phải chịu đựng sự lạm dụng hoặc chấn thương, bao gồm cả chứng kiến sự kiện chấn thương, có nguy cơ cao phát triển rối loạn lo âu tổng quát tại một số thời điểm trong cuộc sống.
Bệnh tật. Có một tình trạng bệnh mãn tính hay bệnh tật nghiêm trọng, chẳng hạn như ung thư, có thể dẫn đến nỗi lo thường trực về tương lai, điều trị và tài chính.
Căng thẳng. Một sự kiện lớn hoặc một số tình huống căng thẳng nhỏ hơn có thể gây ra lo lắng quá mức.


Nhân cách một số người dễ bị rối loạn lo âu hơn những người khác. Ngoài ra, một số rối loạn nhân cách, chẳng hạn như rối loạn nhân cách ranh giới, cũng có thể được liên kết đến rối loạn lo âu tổng quát.

Di truyền học. Rối loạn lo âu có thể di truyền trong gia đình.

Lạm dụng lạm dụng ma túy hoặc rượu có thể làm trầm trọng thêm rối loạn lo âu tổng quát. Caffeine và nicotine cũng có thể làm tăng sự lo lắng.

Các biến chứng

Rối loạn lo âu tổng quát không có gì nhiều hơn chỉ làm cho lo lắng. Nó cũng có thể dẫn đến, hoặc tồi tệ hơn điều kiện sức khỏe tinh thần và thể chất khác, bao gồm:

http://yhoccotruyensaigon.com/dieu-tri-roi-loan-lo-au-tram-tram-trong-bao-lau-495.html

Trầm cảm.
Lạm dụng thuốc.
Khó ngủ (mất ngủ).
Các vấn đề về tiêu hóa hay đường ruột.
Nhức đầu.
Nghiến răng (bệnh nghiến răng).
Kiểm tra và chẩn đoán


Nhà cung cấp dịch vụ sức khỏe tâm thần sẽ có một số bước để giúp chẩn đoán rối loạn lo âu tổng quát. Có thể bắt đầu bằng cách đặt câu hỏi chi tiết về các triệu chứng và lịch sử y tế. Trong một số trường hợp, nhà cung cấp dịch vụ sức khỏe tâm thần sử dụng bảng câu hỏi tâm lý để giúp xác định những gì đang xảy ra. Bác sĩ cũng có thể làm kiểm tra vật lý để tìm dấu hiệu lo lắng có thể liên kết với một điều kiện y tế cơ bản.

Để được chẩn đoán mắc chứng rối loạn lo âu tổng quát, phải đáp ứng các tiêu chí nêu ra trong hướng dẫn chẩn đoán và thống kê các rối loạn tâm thần (DSM). Hướng dẫn này được công bố bởi Hiệp hội Tâm thần Mỹ và được sử dụng bởi các chuyên gia sức khỏe tâm thần để chẩn đoán các điều kiện tinh thần và các công ty bảo hiểm bồi hoàn cho điều trị.

Các tiêu chí sau đây phải được đáp ứng cho một chẩn đoán rối loạn lo âu tổng quát:

Quá nhiều lo lắng và lo lắng về một số sự kiện hoặc hoạt động của hầu hết các ngày trong tuần, ít nhất sáu tháng.

Khó khăn trong việc kiểm soát các cảm xúc lo lắng.

Lo âu hoặc lo lắng là nguyên nhân gây căng thẳng đáng kể hoặc gây trở ngại cho cuộc sống hàng ngày.

Lo lắng không liên quan đến một tình trạng sức khỏe tâm thần, chẳng hạn như các cuộc tấn công hoảng loạn, lạm dụng chất hoặc rối loạn căng thẳng hậu chấn thương tâm lý (PTSD).

Ít nhất ba trong số các triệu chứng sau đây ở người lớn và một trong những điều sau đây ở trẻ em: bồn chồn, mệt mỏi, khó tập trung, khó chịu, cơ bắp căng thẳng hoặc khó ngủ.

Rối loạn lo âu tổng quát thường xảy ra cùng với vấn đề sức khỏe tâm thần khác, có thể làm cho chẩn đoán và điều trị khó khăn hơn. Một số rối loạn thường xảy ra với rối loạn lo âu tổng quát bao gồm:


Ám ảnh.
Rối loạn hoảng sợ.
Trầm cảm.
Lạm dụng thuốc.
Rối loạn stress sau chấn thương.


Nếu bác sĩ nghi ngờ lo lắng có thể có một nguyên nhân y tế, họ có thể yêu cầu xét nghiệm máu hoặc nước tiểu hoặc xét nghiệm khác để tìm dấu hiệu của một vấn đề vật lý.

Phương pháp điều trị và thuốc

Hai phương pháp điều trị chính cho chứng rối loạn lo âu tổng quát là thuốc và tâm lý trị liệu. Thậm chí có thể hưởng lợi nhiều hơn từ sự kết hợp của cả hai. Nó có thể mất một số thử và sai để khám phá ra chính xác những gì các phương pháp điều trị làm việc tốt nhất cho.

Thuốc

Một số loại khác nhau của các loại thuốc được sử dụng để điều trị rối loạn lo âu tổng quát:

Thuốc chống trầm cảm. Các loại thuốc này ảnh hưởng đến hoạt động của hóa chất trong não (chất dẫn truyền thần kinh) có một vai trò trong rối loạn lo âu. Ví dụ thuốc chống trầm cảm được sử dụng để điều trị rối loạn lo âu tổng quát bao gồm Paroxetine (Paxil), sertraline (Zoloft), venlafaxine (Effexor).

Buspirone. Thuốc chống lo âu này có thể được sử dụng trên cơ sở liên tục. Như với hầu hết các thuốc chống trầm cảm, nó thường mất đến vài tuần để đầy đủ hiệu quả. Một tác dụng phụ thường gặp của buspirone là cảm giác chóng mặt ngay sau khi dùng. Tác dụng phụ ít gặp hơn bao gồm đau đầu, buồn nôn, căng thẳng và mất ngủ.

Benzodiazepin. Trong trường hợp hạn chế, bác sĩ có thể quy định một trong các thuốc an thần để cứu trợ ngắn hạn các triệu chứng lo âu. Ví dụ như lorazepam (Ativan), diazepam (Valium), chlordiazepoxide (Librium) và alprazolam (Xanax). Benzodiazepines thường chỉ được sử dụng để giảm lo âu cấp tính trên cơ sở ngắn hạn. Có thể thói quen hình thành và có thể gây ra một số tác dụng phụ, bao gồm buồn ngủ, giảm phối hợp cơ, và các vấn đề với sự cân bằng và bộ nhớ.

Trong một số trường hợp, thuốc này không cụ thể được chấp thuận cho rối loạn lo âu tổng quát. Nhãn sử dụng là một thực tế phổ biến và pháp lý của việc sử dụng một loại thuốc để điều trị một điều kiện không cụ thể được liệt kê trên nhãn quy định của nó như là sử dụng được FDA phê chuẩn.

Tâm lý trị liệu

Còn được gọi là liệu pháp nói chuyện và tư vấn tâm lý, tâm lý trị liệu liên quan đến làm việc căng thẳng trong cuộc sống cơ bản và mối quan tâm và thay đổi hành vi. Nó có thể là một điều trị rất hiệu quả đối với lo âu.

Liệu pháp nhận thức hành vi là một trong những loại phổ biến nhất của tâm lý trị liệu cho chứng rối loạn lo âu tổng quát. Nói chung là điều trị ngắn hạn, liệu pháp nhận thức hành vi tập trung vào việc dạy những kỹ năng cụ thể để xác định những suy nghĩ và hành vi tiêu cực và thay thế chúng với những người tích cực. Ngay cả khi một tình huống không mong muốn không thay đổi, có thể làm giảm căng thẳng và giành quyền kiểm soát nhiều hơn đối với cuộc sống bằng cách thay đổi cách phản ứng.

Lối sống và các biện pháp khắc phục

Trong khi hầu hết mọi người mắc chứng rối loạn lo âu tổng quát cần trị liệu tâm lý hoặc thuốc để có được sự lo lắng dưới sự kiểm soát, thay đổi lối sống cũng có thể tạo sự khác biệt. Dưới đây là một vài điều có thể làm:


Tập thể dục hàng ngày. Tập thể dục là giảm căng thẳng mạnh mẽ, có thể cải thiện tâm trạng và có thể giữ cho khỏe mạnh. Tốt nhất nếu phát triển một thói quen thường xuyên và làm việc hầu hết các ngày trong tuần. Bắt đầu chậm và dần dần tăng số lượng và cường độ tập thể dục.
Ăn một chế độ ăn uống lành mạnh. Tránh béo, thức ăn có đường và chế biến. Bao gồm các loại thực phẩm trong chế độ ăn uống giàu axit béo omega-3 và các vitamin B.
Tránh uống rượu và thuốc an thần khác. Có thể làm trầm trọng thêm sự lo lắng.
Sử dụng kỹ thuật thư giãn. Kỹ thuật trực quan, thiền và yoga là những ví dụ của các kỹ thuật thư giãn mà có thể dễ dàng gây lo lắng.
Hãy ngủ ưu tiên. Hãy làm những gì có thể chắc chắn đang nhận được đủ giấc ngủ chất lượng. Nếu không ngủ, gặp bác sĩ.


Thuốc thay thế

Bổ sung nhất định có thể giúp giảm lo lắng, mặc dù nó không rõ ràng bao nhiêu giúp có thể có những tác dụng phụ có thể xảy ra. Một số bổ sung được sử dụng để điều trị lo âu bao gồm:

Kava. Loại thảo dược này được báo cáo để giúp thư giãn mà không làm cho cảm thấy bình thản. Một số nghiên cứu đã liên kết Kava ​​vấn đề về gan, vì vậy nó không phải là một ý tưởng tốt để có nó nếu có một tình trạng gan, uống rượu hàng ngày hoặc uống thuốc có ảnh hưởng đến gan.

Valerian. Phổ biến nhất được sử dụng như là một trợ giúp giấc ngủ, valerian có tác dụng an thần và cũng có thể làm giảm lo âu.

Vitamin B và acid folic. Các dưỡng chất này có thể làm giảm lo lắng bằng cách ảnh hưởng đến việc sản xuất các hóa chất cần thiết cho bộ não hoạt động (chất dẫn truyền thần kinh).

Nói chuyện với bác sĩ trước khi dùng các biện pháp khắc phục hậu quả thảo dược bổ sung để đảm bảo rằng chúng an toàn và không tương tác với bất cứ loại thuốc nào có.